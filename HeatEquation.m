clc;
clear;
%% 
%目标 ： 利用新谱方法求解以下热方程 ：
%  du/dt = d2u/dx2 
%  u(+-1 , t) = 0 
%  u(x , 0) = f(x) 
%  其中时间方向利用一阶显示欧拉方法 ， 时间方向用新谱方法


%% 
% 参数设置
N = 200;
dt =  3 / N^(4);
T = 0.5;
t_end = floor(T / dt);

%% 
% 初值条件
a_0 = chebcoeffs(chebfun('sin( 2 * pi * x) + 1' , N + 1));

%% 
% 微分矩阵
D_2 = convertmat(N + 1, 2, 1) * diffmat(N + 1, 2);

%% 
% 基底转换矩阵
S = convertmat(N + 1, 0, 1);

%% 
% 边值条件
B = [bc(N + 1, 'd', 'l'); bc(N + 1, 'd', 'r')];
BC = [1 ; 1];
P0 = eye(N + 1 , N + 1);
P0(N : N + 1 , 1 : N + 1) = B;

%% N
% 求解
P = dt * S^(-1) * D_2;
a_1 = zeros(N + 1 , 1);
for j = 1 : 1 : t_end
    a_1 = P * a_0 + a_0;
    a_1(N : N + 1 , 1) =  B(: , N : N + 1)^(-1) * (BC - B(: , 1 : N - 1) * a_1(1 : N - 1 , 1));
    a_0 = a_1;
end

%% 
% 图像
M = 1000;
xx = linspace(-1 , 1 , M);
A = zeros(M , N + 1);
A( : , 1) = ones(M , 1);
for n = 1 : 1 : N
   A( : , n + 1) = cos(n * acos(xx')); 
end
y_num1 = A * a_1;

%% Exact solution
y_exact = exp(- 4 * pi * pi * dt * t_end) * sin( 2 * pi * xx') + 1;
plot(xx , y_exact)
hold on 
plot(xx , y_num1)

%% error
err = norm(y_num1 - y_exact , inf);