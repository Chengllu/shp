clc
clear
%% 
%目标 ： 计算用新谱方法计算以下方程的迭代矩阵的谱和伪谱 ：
%  du/dt = du/dx 
%  u(1 , t) = 0 
%  u(x , 0) = f(x) 
%  其中时间方向利用一阶显示欧拉方法 ， 时间方向用新谱方法


%%
% 参数设置
N = 200;
dt = 3.12 / N^(2);

%% 
% 数值初值条件
a_0 = chebcoeffs(chebfun('1 - x' , N + 1));

%%
% 微分矩阵
D_0 = convertmat(N + 1, 1, 0) * diffmat(N + 1, 1);;

%% 
% 基底转换矩阵
S_0 = convertmat(N + 1, 0, 0);
%
%% 
% 边界条件矩阵
P0 = eye(N + 1 , N + 1);
P0(N + 1 , 1 : N) = ones(1 , N);

%%
% 迭代矩阵
P =  P0 ^ (-1) * S_0^(-1) * dt * D_0;

%% 
% 迭代矩阵的谱
lamda = eig(P);
x_P = real(lamda);
y_P = imag(lamda);
plot(x_P,y_P,'.')
hold on

%% 
% 迭代矩阵的伪谱
opts.npts = 100;
opts.ax = [-2.5 1 -1.5 1.5];
opts.levels = [-10:-1]; 
eigtool(P, opts)
