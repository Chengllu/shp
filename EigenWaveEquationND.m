clc;
clear;
%% AIM : d2u/dt2 = d2u/dx2 ; u(1 , t) = 0 ; u_x(-1 , t) = 0 ; u(x , 0) = f(x) de xin pufangfa jiefa
%% canshu shezhi
N = 8;
dt = 5.1 / N^(3);
%
%% erjie weifen juzhen D_0
D_2 = zeros(N + 1 , N + 1);
for n = 1 : 1 : N - 1
   D_2(n , n + 2) = n + 1; 
end
D_2 = 2 * D_2;
%
%% jidi zhuanhuan juzhen S_0 , S_1
S_0 = zeros(N + 1 , N + 1);
S_0(1 , 1) = 1;
for n = 2 : 1 : N + 1
    S_0(n , n) = 0.5;
end
for n = 1 : 1 : N - 1
   S_0(n , n + 2) = -0.5; 
end

S_1 = zeros(N + 1 , N + 1);
S_1(1 , 1) = 1;
for n = 2 : 1 : N + 1
   S_1(n , n) = 1 / n; 
end
for n = 1 : 1 : N - 1
   S_1(n , n + 2) = -1 / (n + 2); 
end
%
%% shijian diedai
B = zeros(2 , N + 1);
for n = 1 : 1 : N + 1
   B(1 , n) = cos((n - 1) * acos(1));
   B(2 , n) = (-1)^(n) * (n - 1) * (n - 1);
end
P0 = eye(N + 1 , N + 1);
P0(N : N + 1 , 1 : N + 1) = B;
P1 = P0 ^ (-1);
P2 = dt^2 * P1 * S_0^(-1) * S_1^(-1) * D_2;%*** diedai juzhen
% P2 = P1 * S_0^(-1) * S_1^(-1) * D_2;
syms x
BB = x * eye(N + 1) - P2;
f(x) = det(BB)
%
%% huatu
lamda = eig(P2);
xxx = real(lamda);
yyy = imag(lamda);
plot(xxx,yyy,'.')
%
%% diedai juzhen de weipu
opts.npts = 100;
opts.ax = [-4.5 1 -2.5 2.5];
opts.levels = [-10:-1]; 
eigtool(P2, opts)