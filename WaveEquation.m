clc;
clear;
%% 
%目标 ： 利用新谱方法求解以下波方程 ：
%  d2u/dt2 = d2u/dx2 
%  u(+-1 , t) = 0 
%  u(x , 0) = f(x) 
%  其中时间方向利用中心差分方法 ， 时间方向用新谱方法


%% 
% 参数设置
N =400;
dt = 1 / N^(2);
T = 2;
t_end = floor(T / dt);

%% 
% 数值初值条件
a_0 = chebcoeffs(chebfun('exp(-200 * (x)^2)' , N + 1));
a_1 = chebcoeffs(chebfun(@(x)exp(-200 * (x - dt)^2) , N + 1));

%% 
% 微分矩阵
D_2 = convertmat(N + 1, 2, 1) * diffmat(N + 1, 2);

%% 
% 基底转换矩阵
S = convertmat(N + 1, 0, 1);

%% 
% 边界条件矩阵
B = [bc(N + 1, 'd', 'l'); bc(N + 1, 'd', 'r')];
P0 = eye(N + 1 , N + 1);
P0(N : N + 1 , 1 : N + 1) = B;

%%
% 求解
a_2 = zeros(N + 1 , 1);
P2 = dt^2 * P0 ^ (-1) * S^(-1) * D_2;
for j = 1 : 1 : t_end
    a_2 = P2 * a_1 + 2 * a_1 - a_0;
    a_0 = a_1;
    a_1 = a_2;
end
%
%% 
% 画图
M = 1000;
xx = linspace(-1 , 1 , M);
A = zeros(M , N + 1);
A( : , 1) = ones(M , 1);
for n = 1 : 1 : N
   A( : , n + 1) = cos(n * acos(xx')); 
end
y_num = A * a_2;
% plot(xx , y_num)

%%
% 误差
y_exact = -exp(-200 * (xx').^2);
err = norm(y_num - y_exact , inf)